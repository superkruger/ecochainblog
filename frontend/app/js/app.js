'use strict';

/* App Module */

var ecochainblogApp = angular.module('ecochainblogApp', [
  'ngRoute',
  'ecochainblogAnimations',
  'ecochainblogControllers',
  'ecochainblogFilters',
  'ecochainblogServices',
  'angular-momentjs'
]);

ecochainblogApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/articles', {
        templateUrl: 'partials/article-list.html',
        controller: 'ArticleListCtrl'
      }).
      when('/articles/:listUrl', {
        templateUrl: 'partials/article-list.html',
        controller: 'ArticleListCtrl'
      }).
      when('/article/:articleUrl/:listUrl', {
        templateUrl: 'partials/article-detail.html',
        controller: 'ArticleDetailCtrl'
      }).
      otherwise({
        redirectTo: '/articles'
      });
  }]);
