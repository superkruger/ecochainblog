'use strict';

/* Filters */

var ecochainblogFilters = angular.module('ecochainblogFilters', []);

ecochainblogFilters.filter('nextLink', function() {
  return function(resource) {
    return getHrefLinkForRel(resource, 'next');
  };
});

ecochainblogFilters.filter('previousLink', function() {
  return function(resource) {
    return getHrefLinkForRel(resource, 'previous');
  };
});

ecochainblogFilters.filter('selfLink', function() {
  return function(resource) {
    return getHrefLinkForRel(resource, 'self');
  };
});

function getHrefLinkForRel(resource, rel) {
  if(resource) {
    var links = resource.links;

    if(links && links.length > 0) {
      for (var i=0; i<links.length; i++) {
        if (links[i].rel == rel) {
          return window.btoa(links[i].href);
        }
      }
    }
    
    return null;
  }
}