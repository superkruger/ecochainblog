'use strict';

/* Services */

var ecochainblogServices = angular.module('ecochainblogServices', ['ngResource']);

// server api base url
var baseUrl = 'http://localhost:8080';

ecochainblogServices.factory('ArticlesService', function($http) {
  var ArticlesService = {
    getList: function(url) {
      if (!url) {
        url = baseUrl + '/articles';
      }
      var promise = $http.get(url).then(function (response) {
        console.log(response);
        return response.data;
      });
      // Return the promise to the controller
      return promise;
    },
    
    search: function(query) {
      var url = baseUrl + '/articles/search?query=' + query;
      var promise = $http.get(url).then(function (response) {
        console.log(response);
        return response.data;
      });
      // Return the promise to the controller
      return promise;
    },
    
    get: function(url) {
      var promise = $http.get(url).then(function (response) {
        console.log(response);
        return response.data;
      });
      // Return the promise to the controller
      return promise;
    }
  };
  return ArticlesService;
});
