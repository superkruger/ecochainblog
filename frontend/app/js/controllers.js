'use strict';

/* Controllers */

var ecochainblogControllers = angular.module('ecochainblogControllers', []);

ecochainblogControllers.controller('ArticleListCtrl', ['$scope', '$routeParams', '$filter', '$moment', 'ArticlesService',
  function($scope, $routeParams, $filter, $moment, ArticlesService) {
    
    var url = $routeParams.listUrl;
    if (url) {
      url = window.atob(url);
    }
    
    ArticlesService.getList(url).then(function(d) {
      $scope.listUrl = $filter('selfLink')(d);
      $scope.listResult = d;
    });
    
    $scope.snippet = function(text) {
      return text.substring(0, 100) + '...';
    };
    
    $scope.encode = function(text) {
      return window.btoa(text);
    };
    
    $scope.date = function(dateTimeText) {
      return $moment(dateTimeText).format("MMMM Do YYYY");
    }
    
    $scope.$watch('searchString', function(newVal, oldVal) {
      
      if (newVal && newVal.length > 0) {
        ArticlesService.search(newVal).then(function(d) {
          $scope.searchResult = d;
        });
      } else {
        $scope.searchResult = null;
      }
    });
  }]);

ecochainblogControllers.controller('ArticleDetailCtrl', ['$scope', '$routeParams', '$moment', 'ArticlesService',
  function($scope, $routeParams, $moment, ArticlesService) {
    ArticlesService.get(window.atob($routeParams.articleUrl)).then(function(d) {
      $scope.article = d;
      $scope.listUrl = $routeParams.listUrl;
    });
    
    $scope.date = function(dateTimeText) {
      return $moment(dateTimeText).format("MMMM Do YYYY");
    }
    
  }]);
