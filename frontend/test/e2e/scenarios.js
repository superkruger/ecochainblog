'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('EcoChainBlog App', function() {

  it('should redirect index.html to index.html#/articles', function() {
    browser.get('app/index.html');
    browser.getLocationAbsUrl().then(function(url) {
        expect(url).toEqual('/articles');
      });
  });


  describe('Article list view', function() {

    beforeEach(function() {
      browser.get('app/index.html#/articles');
    });


    it('should display articles', function() {

      var articleList = element.all(by.repeater('article in listResult.results'));
      
      expect(articleList.count()).toBe(10);
    });


    it('should search', function() {

      var articleList = element.all(by.repeater('article in searchResult.results'));
      var search = element(by.model('searchString'));

      search.sendKeys('boy');

      expect(articleList.count()).toBe(10);

    });

  });
});
