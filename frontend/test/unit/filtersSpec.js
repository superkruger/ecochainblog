'use strict';

/* jasmine specs for filters go here */

describe('filter', function() {

  beforeEach(module('ecochainblogFilters'));

  describe('selfLink', function() {
    it('should get self link',
        inject(function(selfLinkFilter) {
      expect(selfLinkFilter(
        {
          "links": [
            {
              "rel": "self",
              "href": "http://somelink"
            }
          ]
        }
      )).toBe(window.btoa('http://somelink'));
    }));
  });
  
  describe('nextLink', function() {
    it('should get next link',
        inject(function(nextLinkFilter) {
      expect(nextLinkFilter(
        {
          "links": [
            {
              "rel": "next",
              "href": "http://somelink"
            }
          ]
        }
      )).toBe(window.btoa('http://somelink'));
    }));
  });
  
  describe('previousLink', function() {
    it('should get previous link',
        inject(function(previousLinkFilter) {
      expect(previousLinkFilter(
        {
          "links": [
            {
              "rel": "previous",
              "href": "http://somelink"
            }
          ]
        }
      )).toBe(window.btoa('http://somelink'));
    }));
  });
});
