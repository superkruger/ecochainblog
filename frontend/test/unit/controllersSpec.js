'use strict';

/* jasmine specs for controllers go here */
describe('EcoChainBlog controllers', function() {
  
  var mockSvc;
  
  mockSvc = jasmine.createSpyObj('ArticlesService', ['getList', 'get']);

  beforeEach(function(){
    this.addMatchers({
      toEqualData: function(expected) {
        return angular.equals(this.actual, expected);
      }
    });
  });

  beforeEach(module('ecochainblogApp'));
  beforeEach(module('ecochainblogServices'));

  // Detail
  
  describe('ArticleListCtrl', function(){
    var scope, routeParams, filter, moment, ctrl,
        
        articleListData = function() {
          return {
            "results": [
              {
                  "heading": "heading",
                  "createDateTime": "2015-09-26T13:16:22.689",
                  "body": "body",
                  "links": [
                      {
                          "rel": "self",
                          "href": "http://article.link"
                      }
                  ]
              }
            ],
            "links": [
              {
                  "rel": "self",
                  "href": "http://self.link"
              },
              {
                  "rel": "next",
                  "href": "http://next.link"
              },
              {
                  "rel": "previous",
                  "href": "http://previous.link"
              }
            ]
          }
        };

    beforeEach(inject(function($rootScope, $controller, $routeParams, $filter, $moment, $q) {
      scope=$rootScope.$new();
      routeParams = $routeParams;
      filter=$filter;
      moment=$moment;
      
      routeParams.listUrl = 'someUrl';
      
      mockSvc.getList.andReturn($q.when(articleListData()));
      
      ctrl = $controller('ArticleListCtrl', {
        $scope: scope,
        $routeParams: routeParams,
        $filter: filter,
        $moment: moment,
        ArticlesService: mockSvc
      });
    }));

    it('should get articles list result', function() {
      
      scope.$digest();
      expect(mockSvc.getList).toHaveBeenCalled();
      
      expect(scope.listResult).toEqualData(articleListData());
      expect(scope.listUrl).toEqualData(window.btoa('http://self.link'));
    });

  });
  
  /// Detail
  
  describe('ArticleDetailCtrl', function(){
    var scope, routeParams, filter, moment, ctrl,
        
        articleData = function() {
          return {
              "heading": "heading",
              "createDateTime": "2015-09-26T13:16:22.689",
              "body": "body",
              "links": [
                  {
                      "rel": "self",
                      "href": "http://article.link"
                  }
              ]
          } 
        };

    beforeEach(inject(function($rootScope, $controller, $routeParams, $moment, $q) {
      scope=$rootScope.$new();
      routeParams = $routeParams;
      moment=$moment;
      
      routeParams.articleUrl = 'articleUrl';
      routeParams.listUrl = 'listUrl';
      
      mockSvc.get.andReturn($q.when(articleData()));
      
      ctrl = $controller('ArticleDetailCtrl', {
        $scope: scope,
        $routeParams: routeParams,
        $moment: moment,
        ArticlesService: mockSvc
      });
    }));

    it('should get article', function() {
      
      scope.$digest();
      expect(mockSvc.get).toHaveBeenCalled();
      
      expect(scope.article).toEqualData(articleData());
      expect(scope.listUrl).toEqualData('listUrl');
    });

  });

});
