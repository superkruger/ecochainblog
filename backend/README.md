# EcoChain Blog Service

## To get started, you will need:
* JDK 1.7.0
* MongoDB

##
## Quick Run
##

An executable jar has been included for convenience.

## 1. To run from the executable jar local MongoDB: (depends on step 2, if mongodb not yet started)
* $ java -jar ecochainblog-1.0.0.jar

## 2. To start MongoDB: (depends on step 3, if mongodb not yet installed)
* $ mongod --dbpath ./data

## 3. To install MongoDB: (depends on step 4, if homebrew not yet installed)
* $ brew install mongodb

## 4. To install brew (http://brew.sh):
* $ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

## 5. List Query API
* $ curl http://localhost:8080/articles

## 6. Search Query API
* $ curl http://localhost:8080/articles/search?query=boy

## 7. Reset database
* $ rm -rf ./data

##
## Swagger API
##

To view the Swagger API:
* go to http://editor.swagger.io/#/
* File -> Import File: ./src/main/resources/interfaces/EcoChainBlog.yaml

##
## Development
##

## 1. To setup Intellij IDE
* $ ./gradlew idea

## 2. To build:
* $ ./gradlew clean build

## 3. To run against local MongoDB: (depends on step 4, if mongodb not yet started)
* $ ./gradlew bootRun

## 4. To start MongoDB: (depends on step 5, if mongodb not yet installed)
* $ mongod --dbpath ./data

## 5. To install MongoDB: (depends on step 6, if homebrew not yet installed)
* $ brew install mongodb

## 6. To install brew (http://brew.sh):
* $ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"