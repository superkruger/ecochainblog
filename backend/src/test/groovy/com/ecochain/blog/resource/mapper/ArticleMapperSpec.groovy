package com.ecochain.blog.resource.mapper

import com.ecochain.blog.resource.mapper.fixture.MapperFixtures
import org.springframework.data.domain.Page
import spock.lang.Specification

class ArticleMapperSpec extends Specification {

    def testArticleMapper

    def setup() {
        testArticleMapper = new ArticleMapper()
    }

    def 'test buildResourceModel method'() {
        when:
        def result = testArticleMapper.buildResourceModel(domainModel)

        then:
        result.heading == resourceModel.heading
        result.body == resourceModel.body
        result.createDateTime == resourceModel.createDateTime

        where:
        domainModel                   || resourceModel
        MapperFixtures.DOMAIN_ARTICLE || MapperFixtures.RESOURCE_ARTICLE
    }

    def 'test buildSearchResourceModel method'() {
        when:
        def result = testArticleMapper.buildSearchResourceModel(domainModel)

        then:
        result.heading == resourceModel.heading
        result.createDateTime == resourceModel.createDateTime
        result.score == resourceModel.score

        where:
        domainModel                   || resourceModel
        MapperFixtures.DOMAIN_ARTICLE || MapperFixtures.RESOURCE_SEARCH_ARTICLE
    }

    def 'test buildResourceModels method'() {
        given:
        def page = Mock(Page)
        page.totalElements >> 20
        page.getContent() >> [MapperFixtures.DOMAIN_ARTICLE, MapperFixtures.DOMAIN_ARTICLE]

        when:
        def result = testArticleMapper.buildResourceModels(page)

        then:
        result.count == 20
        result.results == [MapperFixtures.RESOURCE_ARTICLE, MapperFixtures.RESOURCE_ARTICLE]
    }

    def 'test buildSummaryResourceModels method'() {
        given:
        def articles = [MapperFixtures.DOMAIN_ARTICLE, MapperFixtures.DOMAIN_ARTICLE]

        when:
        def result = testArticleMapper.buildSummaryResourceModels(articles)

        then:
        result.results == [MapperFixtures.RESOURCE_SEARCH_ARTICLE, MapperFixtures.RESOURCE_SEARCH_ARTICLE]
    }
}
