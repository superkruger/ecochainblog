package com.ecochain.blog.resource.mapper.fixture

import com.ecochain.blog.domain.model.DomainArticle
import com.ecochain.blog.resource.model.FullArticle
import com.ecochain.blog.resource.model.SearchArticle
import org.joda.time.LocalDateTime

class MapperFixtures {

    static final DOMAIN_ARTICLE = new DomainArticle(
        id: 1,
        heading: 'Heading ABC',
        body: 'Body XYZ',
        createDateTime: LocalDateTime.now(),
        score: 2
    )

    static final RESOURCE_ARTICLE = new FullArticle(
        domainId: DOMAIN_ARTICLE.id,
        heading: DOMAIN_ARTICLE.heading,
        body: DOMAIN_ARTICLE.body,
        createDateTime: DOMAIN_ARTICLE.createDateTime
    )

    static final RESOURCE_SEARCH_ARTICLE = new SearchArticle(
            domainId: DOMAIN_ARTICLE.id,
            heading: DOMAIN_ARTICLE.heading,
            score: 2,
            createDateTime: DOMAIN_ARTICLE.createDateTime
    )
}
