package com.ecochain.blog.resource.api

import com.ecochain.blog.domain.model.DomainArticle
import com.ecochain.blog.resource.mapper.ArticleMapper
import com.ecochain.blog.service.ArticleService
import org.hamcrest.Matchers
import org.joda.time.LocalDateTime
import org.springframework.data.domain.Page
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification
import spock.lang.Unroll

class ArticlesApiSpec extends Specification {

    static final CREATE_DATE_TIME = LocalDateTime.now()

    static final SEARCH_API = '/articles/search'
    static final LIST_API = '/articles'
    static final GET_API = '/articles/'

    static final HEADING_ATTR = 'heading'
    static final BODY_ATTR = 'body'
    static final CREATE_DATE_TIME_ATTR = 'createDateTime'
    static final COUNT_PATH = '$.count'
    static final LINKS_PATH = '$.links'
    static final ARTICLES_PATH = '$.results'
    static final ARTICLE_BY_ATTR_PATH = ARTICLES_PATH + '[?(@%s==%s)]'

    static final QUERY_PARAM = 'query'
    static final OFFSET_PARAM = 'offset'
    static final LIMIT_PARAM = 'limit'

    def mockArticleService
    def testApi
    def mockMvc

    def setup() {
        mockArticleService = Mock(ArticleService)
        testApi = new ArticlesApi(
                articleService: mockArticleService,
                articleMapper: new ArticleMapper()
        )
        mockMvc = MockMvcBuilders.standaloneSetup(testApi).build()
    }

    def '/articles/search returns article'() {
        given:
        def article1 = createArticle(1, "heading", "body")
        def articles = [article1]

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get(SEARCH_API)
                .param(QUERY_PARAM, "heading")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())

        then:
        1 * mockArticleService.searchArticles(_, _, _) >> articles

        then:
        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath(ARTICLES_PATH, Matchers.hasSize(articles.size())))
        summaryResponseChecker(response, article1)
    }

    def '/articles/search returns articles'() {
        given:
        def article1 = createArticle(1, "heading", "body")
        def article2 = createArticle(2, "heading", "body")
        def articles = [article1, article2]

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get(SEARCH_API)
                .param(QUERY_PARAM, "heading")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())

        then:
        1 * mockArticleService.searchArticles(_, _, _) >> articles

        then:
        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath(ARTICLES_PATH, Matchers.hasSize(articles.size())))
        articles.each {
            summaryResponseChecker(response, it)
        }
    }

    @Unroll
    def '/articles returns paginated articles (scenario #scenario)'() {
        def article = createArticle(1, "heading", "body")
        def articles = [article, article, article, article, article, article, article, article, article, article,
                        article, article, article, article, article, article, article, article, article, article]
        def page = Mock(Page)
        page.getContent() >> articles
        page.totalElements >> articles.size()

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get(LIST_API)
                .contentType(MediaType.APPLICATION_JSON)
                .param(OFFSET_PARAM, offset as String)
                .param(LIMIT_PARAM, limit as String))
                .andDo(MockMvcResultHandlers.print())

        then:
        if (ok) {
            1 * mockArticleService.getArticles(offset, limit) >> page
            if (offset >= articles.size()) {
                1 * page.getContent() >> []
            } else {
                1 * page.getContent() >> articles.subList(offset, Math.min(articles.size(), offset + limit))
            }
            1 * page.getTotalElements() >> articles.size()
        }

        then:
        if (ok) {
            response.andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.jsonPath(COUNT_PATH, Matchers.is(articles.size())))
                    .andExpect(MockMvcResultMatchers.jsonPath(ARTICLES_PATH, Matchers.hasSize(count)))
        } else {
            response.andExpect(MockMvcResultMatchers.status().isBadRequest())
        }

        where:
        scenario | offset | limit | ok    | count
        1        | 0      | 5     | true  | 5
        2        | 5      | 10    | true  | 10
        3        | 10     | 20    | true  | 10
        4        | 100    | 5     | true  | 0
        5        | 'str'  | 5     | false | 0
        6        | 0      | 'str' | false | 0
        7        | 'str'  | 'str' | false | 0
    }

    def '/articles/id returns article'() {
        def article = createArticle(1, "heading", "body")

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get(GET_API + "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())

        then:
        1 * mockArticleService.getArticle(_) >> article

        then:
        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))

    }


    def '/articles/id returns 404'() {

        when:
        mockMvc.perform(MockMvcRequestBuilders.get(GET_API + "2")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())

        then:
        1 * mockArticleService.getArticle(_) >> null

        then:
        thrown(Exception)

    }

    def createArticle(id, heading, body) {
        new DomainArticle(
                id: id,
                heading: heading,
                body: body,
                createDateTime: CREATE_DATE_TIME,
                score: 2
        )
    }

    def responseChecker(response, article) {
        response
                .andExpect(MockMvcResultMatchers.jsonPath(ARTICLE_BY_ATTR_PATH, HEADING_ATTR, article.heading).exists())
                .andExpect(MockMvcResultMatchers.jsonPath(ARTICLE_BY_ATTR_PATH, BODY_ATTR, article.body).exists())
                .andExpect(MockMvcResultMatchers.jsonPath(ARTICLE_BY_ATTR_PATH, CREATE_DATE_TIME_ATTR,
                article.createDateTime).exists())
    }


    def summaryResponseChecker(response, article) {
        response
                .andExpect(MockMvcResultMatchers.jsonPath(ARTICLE_BY_ATTR_PATH, HEADING_ATTR, article.heading).exists())
                .andExpect(MockMvcResultMatchers.jsonPath(ARTICLE_BY_ATTR_PATH, CREATE_DATE_TIME_ATTR,
                article.createDateTime).exists())
    }
}
