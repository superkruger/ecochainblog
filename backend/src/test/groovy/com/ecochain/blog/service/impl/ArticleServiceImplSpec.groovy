package com.ecochain.blog.service.impl

import com.ecochain.blog.domain.model.DomainArticle
import com.ecochain.blog.domain.repo.ArticleRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.core.MongoTemplate
import spock.lang.Specification

class ArticleServiceImplSpec extends Specification {

    def mockArticleRepo = Mock(ArticleRepository);
    def mockTemplate = Mock(MongoTemplate);

    ArticleServiceImpl testService

    def article1
    def articleIdCounter

    def setup() {
        testService = new ArticleServiceImpl(mockArticleRepo, mockTemplate)

        articleIdCounter = 1;

        article1 = createArticle()

    }

    def "it finds articles"() {
        given:
        def testResults = Mock(Page)
        testResults.totalElements >> 1
        testResults.getContent() >> [article1]

        when:
        def result = testService.getArticles(0, 5)

        then: "ArticleRepository.findAll() is called"
        1 * mockArticleRepo.findAll(_) >> testResults

        then: "a page of results is returned"
        result.totalElements == testResults.totalElements

        then: "no exceptions is thrown"
        notThrown(Throwable)
    }

    def createArticle() {
        new DomainArticle(
                id: articleIdCounter++
        )
    }

}
