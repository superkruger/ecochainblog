package com.ecochain.blog.util;

import com.ecochain.blog.util.fixture.FixturesLoader;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("test")
@Component
public class FixturesLoaderImpl implements FixturesLoader {
    @Override
    public void init() {

    }
}
