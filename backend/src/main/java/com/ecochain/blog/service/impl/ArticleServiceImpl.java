package com.ecochain.blog.service.impl;

import com.ecochain.blog.domain.model.DomainArticle;
import com.ecochain.blog.domain.repo.ArticleRepository;
import com.ecochain.blog.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Term;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    private final ArticleRepository articleRepository;

    private final MongoTemplate template;

    @Autowired
    public ArticleServiceImpl(final ArticleRepository productRepo, final MongoTemplate template) {
        this.articleRepository = productRepo;
        this.template = template;
    }

    @Override
    public Page<DomainArticle> getArticles(Integer offset, Integer limit) {

        return articleRepository.findAll(new PageRequest(offset, limit, Sort.Direction.DESC, "createDateTime"));
    }

    @Override
    public List<DomainArticle> searchArticles(Integer offset, Integer limit, String query) {

        if (query == null) {
            query = "";
        }

        TextCriteria criteria = TextCriteria.forDefaultLanguage()
                .matchingPhrase(query);

        Query q = TextQuery.queryText(criteria)
                .sortByScore()
                .with(new PageRequest(offset, limit));

        List<DomainArticle> articles = template.find(q, DomainArticle.class);

        return articles;
    }

    @Override
    public DomainArticle getArticle(String articleId) {
        return articleRepository.findOne(articleId);
    }



}
