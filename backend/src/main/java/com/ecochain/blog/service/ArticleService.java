package com.ecochain.blog.service;

import com.ecochain.blog.domain.model.DomainArticle;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ArticleService {

    /**
     * Get all articles.
     * @return the page of articles
     */
    Page<DomainArticle> getArticles(Integer offset, Integer limit);

    /**
     * Search articles by text.
     * @return the list of matching articles
     */
    List<DomainArticle> searchArticles(Integer offset, Integer limit, String query);

    /**
     * Get article by domainId.
     * @param articleId article domainId
     * @return the article
     */
    DomainArticle getArticle(String articleId);

}
