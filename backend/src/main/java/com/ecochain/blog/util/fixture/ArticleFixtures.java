package com.ecochain.blog.util.fixture;

import com.ecochain.blog.domain.model.DomainArticle;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ArticleFixtures {

    public static final LocalDateTime NOW = LocalDateTime.now();

    static final int NO_WORDS = 5;
    static final String SPACE = " ";
    static final String PERIOD = ".";

    static final String ARTICLE[] = {"the", "a", "one", "some", "any"};
    static final String NOUN[] = {"boy", "girl", "dog", "town", "car"};
    static final String VERB[] = {"drove", "jumped", "ran", "walked", "skipped"};
    static final String PREPOSITION[] = {"to", "from", "over", "under", "on"};

    static Random r = new Random();

    private ArticleFixtures() {
    }

    public static List<DomainArticle> getArticles() {
        List<DomainArticle> list = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            list.add(getArticle(NOW.minusDays(i)));
        }

        return list;
    }

    private static DomainArticle getArticle(LocalDateTime createDateTime) {
        DomainArticle article = new DomainArticle();
        article.setHeading(randomText(1));
        article.setBody(randomText(20));
        article.setCreateDateTime(createDateTime);
        return article;
    }

    static int rand() {
        int ri = r.nextInt() % NO_WORDS;
        if (ri < 0)
            ri += NO_WORDS;
        return ri;
    }

    private static String randomText(int sentences) {

        StringBuilder text = new StringBuilder();
        for (int i = 0; i < sentences; i++) {
            StringBuilder sentence = new StringBuilder();

            String first = ARTICLE[rand()];
            sentence.append(first);
            sentence.replace(0, 1, String.valueOf(first.charAt(0)));
            sentence.append(SPACE + NOUN[rand()] + SPACE);
            sentence.append(VERB[rand()] + SPACE + PREPOSITION[rand()]);
            sentence.append(SPACE + ARTICLE[rand()] + SPACE + NOUN[rand()]);
            sentence.append(PERIOD + SPACE);
            System.out.println(sentence.toString());
            text.append(sentence);
        }

        return text.toString();
    }
}
