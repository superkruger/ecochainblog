package com.ecochain.blog.util.fixture.impl;

import com.ecochain.blog.domain.model.DomainArticle;
import com.ecochain.blog.domain.repo.ArticleRepository;
import com.ecochain.blog.util.fixture.ArticleFixtures;
import com.ecochain.blog.util.fixture.FixturesLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@Profile(value = {"local"})
public class FixturesLoaderImpl implements FixturesLoader {

    @Autowired
    private ArticleRepository articleRepository;

    @PostConstruct
    @Transactional
    public void init() {
        List<DomainArticle> articles = ArticleFixtures.getArticles();
        for (DomainArticle article : articles) {
            articleRepository.save(article);
        }

    }

}
