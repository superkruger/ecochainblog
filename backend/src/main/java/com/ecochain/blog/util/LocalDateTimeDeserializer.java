package com.ecochain.blog.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.joda.time.LocalDateTime;

import java.io.IOException;

public class LocalDateTimeDeserializer extends StdDeserializer<LocalDateTime> {

    public LocalDateTimeDeserializer() {
        super(LocalDateTime.class);
    }

    @Override
    public LocalDateTime deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        LocalDateTime result = null;

        while (jp.nextToken() != null) {
            String name = jp.getCurrentName();
            if ("iso".equals(name)) {
                // getArticle value token
                jp.nextToken();
                // result = sdf.parse(jp.getText());
                result = LocalDateTime.parse(jp.getText());
                // consume last token
                jp.nextToken();
                // for some reason the entire JSON string is passed in
                // break to keep from consuming the rest
                break;
            }
        }

        return result;
    }

    @Override
    public LocalDateTime deserialize(JsonParser jp, DeserializationContext ctxt, LocalDateTime intoValue)
            throws IOException, JsonProcessingException {
        return deserialize(jp, ctxt);
    }
}
