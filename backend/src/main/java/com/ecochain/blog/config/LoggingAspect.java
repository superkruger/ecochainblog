package com.ecochain.blog.config;

import com.google.common.base.Stopwatch;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Aspect
@Component
public class LoggingAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);

    @Around("execution(* com.ecochain.blog.resource.api.*.*(..))")
    public Object logApiAccess(final ProceedingJoinPoint point) throws Throwable {
        final Stopwatch stopWatch = Stopwatch.createStarted();
        final String methodName = point.getSignature().toShortString();
        LOGGER.debug("Entered {}", methodName);
        final Object proceed = point.proceed();
        stopWatch.stop();
        LOGGER.debug("Exiting {} in {}ms]", methodName, stopWatch.elapsed(TimeUnit.MILLISECONDS));
        return proceed;
    }
}
