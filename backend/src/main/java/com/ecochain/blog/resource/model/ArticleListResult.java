package com.ecochain.blog.resource.model;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

public class ArticleListResult extends ResourceSupport {

    private long count = 0L;

    private List<FullArticle> results = new ArrayList<>();

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<FullArticle> getResults() {
        return results;
    }

    public void setResults(List<FullArticle> results) {
        this.results = results;
    }
}
