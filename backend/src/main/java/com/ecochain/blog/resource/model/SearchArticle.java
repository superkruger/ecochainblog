package com.ecochain.blog.resource.model;

public class SearchArticle extends SummaryArticle {

    protected Float score;

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }
}
