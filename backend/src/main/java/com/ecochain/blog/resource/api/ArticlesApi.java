package com.ecochain.blog.resource.api;

import com.ecochain.blog.domain.model.DomainArticle;
import com.ecochain.blog.resource.mapper.ArticleMapper;
import com.ecochain.blog.resource.model.ArticleListResult;
import com.ecochain.blog.resource.model.ArticleSearchResult;
import com.ecochain.blog.resource.model.FullArticle;
import com.ecochain.blog.resource.model.SummaryArticle;
import com.ecochain.blog.service.ArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
@RequestMapping(value = "/articles", produces = {APPLICATION_JSON_VALUE})
public class ArticlesApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArticlesApi.class);

    /* Default offset and page size */
    private static final int DEFAULT_OFFSET = 0;
    private static final int DEFAULT_LIMIT = 10;

    private static final HttpHeaders headers = new HttpHeaders();

    static {
        // allow local npm access
        // temporary solution for CORS issues
        headers.set("Access-Control-Allow-Origin", "http://localhost:8000");
    }

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ArticleMapper articleMapper;


    /**
     * Search for Articles
     *
     * @param offset offset of the page to return, default 0
     * @param limit  limit of results per page, default 10
     * @param query  exact query to match heading or body
     * @return ArticleSearchResult
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public HttpEntity<ArticleSearchResult> searchArticles(
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "query", required = true) String query) {

        if (offset == null) {
            offset = DEFAULT_OFFSET;
        }

        if (limit == null) {
            limit = DEFAULT_LIMIT;
        }

        LOGGER.debug("Search articles: offset={}, limit={}, query={}", offset, limit, query);

        ArticleSearchResult searchResult = articleMapper.buildSummaryResourceModels(
                articleService.searchArticles(offset, limit, query));

        searchResult.add(linkTo(methodOn(ArticlesApi.class).searchArticles(offset, limit, query)).withSelfRel());

        for (SummaryArticle article : searchResult.getResults()) {
            addSelfLink(article);
        }

        return new ResponseEntity<>(searchResult, headers, HttpStatus.OK);
    }

    /**
     * Get all articles
     *
     * @param offset offset of the page to return, default 0
     * @param limit  limit of results per page, default 10
     * @return ArticleListResult
     */
    @RequestMapping(method = RequestMethod.GET)
    public HttpEntity<ArticleListResult> getArticles(
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {

        if (offset == null) {
            offset = DEFAULT_OFFSET;
        }

        if (limit == null) {
            limit = DEFAULT_LIMIT;
        }

        LOGGER.debug("Get articles: offset={}, limit={}", offset, limit);

        Page<DomainArticle> page = articleService.getArticles(offset, limit);

        ArticleListResult list = articleMapper.buildResourceModels(page);

        list.add(linkTo(methodOn(ArticlesApi.class).getArticles(offset, limit)).withSelfRel());

        if (page.hasNext()) {
            list.add(linkTo(methodOn(ArticlesApi.class).getArticles(offset + 1, limit)).withRel("next"));
        }

        if (page.hasPrevious()) {
            list.add(linkTo(methodOn(ArticlesApi.class).getArticles(offset - 1, limit)).withRel("previous"));
        }

        for (FullArticle fullArticle : list.getResults()) {
            addSelfLink(fullArticle);
        }

        return new ResponseEntity<>(list, headers, HttpStatus.OK);
    }

    /**
     * Get an article by domainId
     *
     * @param id article domainId
     * @return Article
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public HttpEntity<FullArticle> getArticle(
            @PathVariable(value = "id") String id) {

        LOGGER.debug("Get article: domainId={}", id);

        DomainArticle domainArticle = articleService.getArticle(id);

        if (domainArticle == null) {
            throw new NotFoundException(404, String.format("Article with domainId %s not found", id));
        }

        FullArticle fullArticle = articleMapper.buildResourceModel(domainArticle);
        addSelfLink(fullArticle);

        return new ResponseEntity<>(fullArticle, headers, HttpStatus.OK);
    }

    private void addSelfLink(SummaryArticle summaryArticle) {
        summaryArticle.add(linkTo(methodOn(ArticlesApi.class).getArticle(summaryArticle.getDomainId())).withSelfRel());
    }

}
