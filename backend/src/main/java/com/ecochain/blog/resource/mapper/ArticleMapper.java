package com.ecochain.blog.resource.mapper;

import com.ecochain.blog.domain.model.DomainArticle;
import com.ecochain.blog.resource.api.NotFoundException;
import com.ecochain.blog.resource.model.ArticleListResult;
import com.ecochain.blog.resource.model.ArticleSearchResult;
import com.ecochain.blog.resource.model.FullArticle;
import com.ecochain.blog.resource.model.SearchArticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ArticleMapper {

    private static final Logger LOG = LoggerFactory.getLogger(ArticleMapper.class);

    public ArticleMapper() {
    }

    /**
     * Map a DomainArticle onto a resource Article
     *
     * @param domainArticle
     * @return
     * @throws NotFoundException
     */
    public FullArticle buildResourceModel(DomainArticle domainArticle) throws NotFoundException {
        LOG.debug("buildResourceModel: {}", domainArticle);
        FullArticle resourceModel = new FullArticle();
        resourceModel.setDomainId(domainArticle.getId());
        resourceModel.setHeading(domainArticle.getHeading());
        resourceModel.setBody(domainArticle.getBody());
        resourceModel.setCreateDateTime((domainArticle.getCreateDateTime()));

        return resourceModel;
    }

    /**
     * Map a DomainArticle onto an ArticleSummary
     *
     * @param domainArticle
     * @return
     * @throws NotFoundException
     */
    public SearchArticle buildSearchResourceModel(DomainArticle domainArticle) throws NotFoundException {
        LOG.debug("buildSearchResourceModel: {}", domainArticle);
        SearchArticle resourceModel = new SearchArticle();
        resourceModel.setDomainId(domainArticle.getId());
        resourceModel.setHeading(domainArticle.getHeading());
        resourceModel.setCreateDateTime((domainArticle.getCreateDateTime()));
        resourceModel.setScore(domainArticle.getScore());

        return resourceModel;
    }

    /**
     * Map a Page of DomainArticles onto an ArticleListResult
     *
     * @param domainArticlePage
     * @return
     * @throws NotFoundException
     */
    public ArticleListResult buildResourceModels(Page<DomainArticle> domainArticlePage) throws NotFoundException {
        ArticleListResult listResult = new ArticleListResult();
        if (domainArticlePage != null) {
            List<DomainArticle> domainArticles = domainArticlePage.getContent();
            if (domainArticles != null) {
                List<FullArticle> resourceModels = new ArrayList<>();
                for (DomainArticle domainArticle : domainArticles) {
                    resourceModels.add(buildResourceModel(domainArticle));
                }
                listResult.setCount(domainArticlePage.getTotalElements());
                listResult.setResults(resourceModels);
            }
        }

        return listResult;
    }

    /**
     * Map a List of DomainArticle onto an ArticleSearchResult
     *
     * @param domainArticles
     * @return
     * @throws NotFoundException
     */
    public ArticleSearchResult buildSummaryResourceModels(List<DomainArticle> domainArticles) throws NotFoundException {
        ArticleSearchResult searchResult = new ArticleSearchResult();
        if (domainArticles != null) {
            if (domainArticles != null) {
                List<SearchArticle> resourceModels = new ArrayList<>();
                for (DomainArticle domainArticle : domainArticles) {
                    resourceModels.add(buildSearchResourceModel(domainArticle));
                }
                searchResult.setResults(resourceModels);
            }
        }

        return searchResult;
    }
}
