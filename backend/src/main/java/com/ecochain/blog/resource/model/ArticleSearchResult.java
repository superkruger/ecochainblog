package com.ecochain.blog.resource.model;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

public class ArticleSearchResult  extends ResourceSupport {

    private List<SearchArticle> results = new ArrayList<>();

    public List<SearchArticle> getResults() {
        return results;
    }

    public void setResults(List<SearchArticle> results) {
        this.results = results;
    }
}
