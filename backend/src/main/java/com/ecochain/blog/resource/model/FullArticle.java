package com.ecochain.blog.resource.model;

public class FullArticle extends SummaryArticle {

    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
