package com.ecochain.blog.resource.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    // HTTP 500
    @ExceptionHandler(value = {RuntimeException.class, IllegalArgumentException.class, IllegalStateException.class, ApiException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    protected ResponseEntity<Object> handleError(RuntimeException ex, WebRequest request) {

        return handleExceptionInternal(ex, ex.getLocalizedMessage(),
                null, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }


    // HTTP 404
    @ExceptionHandler(value = {NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    protected ResponseEntity<Object> handleNotFound(NotFoundException ex, WebRequest request) {

        return handleExceptionInternal(ex, ex.getLocalizedMessage(),
                null, HttpStatus.NOT_FOUND, request);
    }
}
