package com.ecochain.blog.domain.model;

import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;

@Document
public class DomainArticle {
    @Id
    private String id;

    /**
     * Indexed for full text search with a weight of twice that of the body
     */
    @TextIndexed(weight=2)
    private String heading;

    /**
     * Indexed for full text search with a normal weight
     */
    @TextIndexed
    private String body;

    private LocalDateTime createDateTime;

    /**
     * readonly score indicates search relevance
     */
    @TextScore
    private Float score;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }
}
