package com.ecochain.blog.domain.repo;

import com.ecochain.blog.domain.model.DomainArticle;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ArticleRepository extends MongoRepository<DomainArticle, String> {

}
